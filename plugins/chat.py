import random
import re

import jieba.posseg as psg
from nonebot import on_command, CommandSession, on_notice, NoticeSession, permission as perm
from nonebot import on_natural_language, NLPSession, IntentCommand

repeat_flag = 0


@on_command('help', aliases=('帮助', '机器人出来'))
async def help_fuc(session: CommandSession):
    menu = "沙雕机器人烫烫为您服务~烫烫烫……"
    menu += "现在开通的功能有：查询天气，复读机（伪），整点报时，地城提醒与解除"
    await session.send(menu)


@on_command('repeat')
async def repeat(session: CommandSession):
    global repeat_flag
    if repeat_flag:
        await session.send(session.state.get('message') or session.current_arg)


@on_command('switch_on', aliases=('复读机启动', '开始复读', '复读机开启'), permission=perm.SUPERUSER)
async def repeat_on(session: CommandSession):
    await session.send('已开启复读模式！')
    global repeat_flag
    repeat_flag = 1


@on_command('switch_off', aliases=('复读机关闭', '停止复读', '复读机停止'), permission=perm.SUPERUSER)
async def repeat_off(session: CommandSession):
    await session.send('已关闭复读模式！')
    global repeat_flag
    repeat_flag = 0


@on_command('greeting')
async def greeting(session: CommandSession):
    respond = [
        "你好",
        "Hi~",
        "おはようございます~",
        "这位客官想聊点什么？",
        "哈罗2号机，烫烫，为您服务~"
    ]
    await session.send(random.choice(respond))


@on_command('poet')
async def poet(session: CommandSession):
    respond = [
        "苟利国家生死矣, 岂因福祸避趋之~",
        "你们还是要学习一个~",
        "不要想着整天搞个大新闻~",
        "Naive!",
        "I'm angry!",
    ]
    await session.send(random.choice(respond))


@on_command('地城提醒')
async def alarm_desc(session: CommandSession):
    respond = "命令格式：輸入!alarm, 后续根据提示輸入"
    await session.send(respond)


@on_command('提醒解除')
async def lift_desc(session: CommandSession):
    respond = "命令格式：輸入!lift, 后续根据提示輸入"
    await session.send(respond)


@on_command('r')
async def dice(session: CommandSession):
    x = 1
    y = 100
    offset = 0
    pattern = r'(?P<x>[1-9]*[0-9]*)(?P<d>d)(?P<y>[1-9]*[0-9]*)(?P<offset>[\+\-][1-9]*[0-9])'
    res = re.search(pattern, session.current_arg.replace(" ", ""))
    print(session.current_arg)
    ans = random.randint(1, y) * x + offset
    ans = "Result = " + str(ans)
    await session.send(ans)


@on_command('counter_attack')
async def counter_attack(session: CommandSession):
    respond = [
        '呸~你才丢人，你全家都丢人！',
        '你说谁辣鸡呢，小菜鸡',
        '汝甚吊，汝母知否？',
        '我才不是弱鸡！',
        '不要骂脏话，小心被雷劈~',
        '瘪犊子（指东北话）',
        '完蛋玩意儿！',
        '呸~你才辣鸡，你全家都辣鸡！',
        '呸~你才弱鸡，你全家都弱鸡！',
        '天道好轮回~苍天饶过谁~',
        '这个人脑子what啦？',
        '帮帮忙，侬的脑子what不啦？',
    ]
    await session.send(random.choice(respond))


@on_command('caixukun')
async def caixukun(session: CommandSession):
    respond = [

        '你才是蔡徐坤，你全家都是rapper',
        '少侠这篮球想必打的极好',
        '你会唱，跳，rap，篮球~',
        '鸡你太美，贝贝~',
        '大家好，我是练习时长两天半的个人练习生，烫烫~'
    ]
    await session.send(random.choice(respond))


@on_command('name')
async def name(session: CommandSession):
    respond = [
        '我是烫烫，是机器人哦~',
        '锟斤拷烫烫烫，就是少侠我~',
        '你们可以叫我烫哥~嘿嘿',
        '大家好，我是练习时长两天半的个人练习生，烫烫~'
    ]
    await session.send(random.choice(respond))


@on_command('encourage')
async def encourage(session: CommandSession):
    respond = [
        '谢谢夸奖~我以后会做得更好',
        '谢谢夸奖~',
        '哼哼，这只是百分之一呢',
        '我才不是弱鸡~只是小了一点而已',
        '客官过奖，嘿嘿嘿',
    ]
    await session.send(random.choice(respond))


@on_command('friends')
async def friends(session: CommandSession):
    friend_name = session.current_arg_text.strip()
    respond = '哦是' + friend_name + '老师啊，我知道我知道（翻字典）'
    await session.send(respond)


@on_command('eat')
async def eat(session: CommandSession):
    respond = [
        '不要吃烫烫呀！咿呀呀呀呀呀呀呀！~',
        '不好意思，机器人是不能吃的！',
        '吃？是说充电吗？',
    ]
    await session.send(random.choice(respond))


@on_notice('group_increase')
async def _(session: NoticeSession):
    await session.send('欢迎新朋友～')


@on_natural_language(keywords={'你好', 'Hi', 'hello'})
async def _(session: NLPSession):
    return IntentCommand(90.0, 'greeting')


@on_natural_language(keywords={'诗', '蛤', '江'}, only_to_me=False)
async def _(session: NLPSession):
    return IntentCommand(90.0, 'poet')


@on_natural_language(keywords='r')
async def _(session: NLPSession):
    return IntentCommand(90.0, 'dice', None, session.msg)


@on_natural_language(keywords={'丢人', '菜', '弱', '废', '辣鸡', '你妈', '尼玛', '傻', '逼',
                               '白痴', '犊子', '蠢', '二货', '智障', '制杖', '完蛋玩意'})
async def _(session: NLPSession):
    # return IntentCommand(90.0, 'greet')
    return IntentCommand(90.0, 'counter_attack')


@on_natural_language(keywords={'蔡', '徐', '坤'})
async def _(session: NLPSession):
    # return IntentCommand(90.0, 'greet')
    return IntentCommand(90.0, 'caixukun')


@on_natural_language(keywords={'不错', '厉害', '牛'})
async def _(session: NLPSession):
    # return IntentCommand(90.0, 'greet')
    return IntentCommand(90.0, 'encourage')


@on_natural_language(keywords={'名字', '烫烫'})
async def _(session: NLPSession):
    # return IntentCommand(90.0, 'greet')
    return IntentCommand(90.0, 'name')


@on_natural_language(keywords={'调戏哈罗'}, )
async def _(session: NLPSession):
    return IntentCommand(90.0, 'trick')


@on_natural_language(keywords={'认识'}, )
async def _(session: NLPSession):
    stripped_msg = session.msg_text.strip()
    words = psg.lcut(stripped_msg)
    friend_name = ""
    for word in words:
        print(word.flag)
        if word.flag in ["nr", "n"]:
            friend_name += word.word
    return IntentCommand(90.0, 'friends', current_arg=friend_name or ' ')


@on_natural_language(keywords={'吃', 'eat', '餐'})
async def _(session: NLPSession):
    return IntentCommand(90.0, 'eat')


@on_natural_language(only_to_me=False)
async def _(session: NLPSession):
    return IntentCommand(80.0, 'repeat', None, session.msg)
