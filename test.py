import random
import re


async def guess_number(session: CommandSession):
    length = 4
    bingo = False
    tries = 8
    a = 0  # correct digit
    b = 0  # correct digit in wrong place

    print('target = ', session.state.get('target'))

    while not bingo and tries:
        parm = 'guess' + str(tries)
        guess = session.get(parm, prompt="请做出你的猜测", arg_filters=[
            extractors.extract_text,  # 提取纯文本部分
            str.strip,  # 去掉两边的空白
            validators.not_empty(),
            validators.match_regex(r'[0-9]{4}', '必须为4位数字'),
            # validators.ensure_true(lambda x: 9999 < int(x) < 100000, '必须猜测10000 ~ 99999中的数字')
        ], at_sender=True)
        await session.send("这是第" + str(9 - tries) + '次猜测:' + guess)
        for i in range(len(session.state.get('target'))):
            if guess[i] == session.state.get('target')[i]:
                a += 1
        for c in guess:
            if c in session.state.get('target'):
                b += 1
        b -= a
        if a == length:
            res = "恭喜你回答正确！"
            bingo = True
            await session.send(res)
            break
        res = "结果是："
        res += str(a) + 'A' + str(b) + 'B'
        await session.send(res)
        tries -= 1
        a = b = 0

    if not bingo:
        res = "抱歉，挑战失败，正确答案是"
        res += session.state.get('target')
        await session.send(res)

    return


if __name__ == '__main__':
    guess_number_game()
